# cocos-sharp-samples-fsharp
F# Examples for CocoSharp (Android mostly).
Under the MIT License.

You can find the CocosSharp Xamarin page including some tutorials here: http://developer.xamarin.com/guides/cross-platform/game_development/cocossharp/

The CocosSharp Xamarin Forum here: https://forums.xamarin.com/categories/cocossharp

And the CocosSharp C# Samples here: https://github.com/mono/cocos-sharp-samples

I also recommend that if you have proficiency in at least reading C++, that you take a look at the Cocos2d-x Programmers Guide v3.3 here: http://www.cocos2d-x.org/programmersguide/

Notice
======
Using the current Xamarin free lincense (as of end of March 2015) will not allow you to build a project with the CocosSharp.Android package as it will be too big. You will need to upgrade to an Indie lincese or higher. I have submitted that feedback to the Xamarin team.

CocosSharpBouncyBallBasic.Android
=================================
Translation (with variations) to F# of the CocosSharp tutorial (C#) example "BouncingGame".
Found here: http://developer.xamarin.com/guides/cross-platform/game_development/cocossharp/first_game/part3/.
This version is a playable game, with endless loop, but you loose score when it resets. Please notice that the packages directory is not included - please use NUGET with your solution to get the CocosSharp.Android package.
