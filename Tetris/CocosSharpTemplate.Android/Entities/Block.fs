﻿namespace CocosSharpTemplate.Android
open System
open CocosSharp

type Block(color:CCColor4B) as blockInstance = 
    inherit CCNode()

    let constants = new Constants()
    let myColor:CCColor4B = color
    let drawNode = new CCDrawNode ()
    let shape = new CCRect(0.0f, 0.0f, constants.BLOCK_SIZE, constants.BLOCK_SIZE)

    member blockInstance.Init() =
        blockInstance.AddChild (drawNode)

        drawNode.PositionX <- 0.0f
        drawNode.PositionY <- 0.0f

        drawNode.DrawRect(shape, myColor, 2.0f, CCColor4B.White)

    member blockInstance.DownStep () =
        blockInstance.PositionY <- blockInstance.PositionY - constants.BLOCK_SIZE;

    member blockInstance.getColor() =
        myColor

    member blockInstance.getLine() = 
        (int)(blockInstance.PositionY / constants.BLOCK_SIZE)

    member blockInstance.IsOverScene() =
        blockInstance.getLine() >= (int)(constants.SCREEN_HEIGHT / constants.BLOCK_SIZE)
