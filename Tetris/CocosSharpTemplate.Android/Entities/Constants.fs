﻿namespace CocosSharpTemplate.Android
open System

type Constants() as constInstance = 
  member constInstance.SCREEN_HEIGHT = 1000.0f
  member constInstance.SCREEN_WIDTH = 500.0f
  member constInstance.BLOCK_SIZE = 50.0f