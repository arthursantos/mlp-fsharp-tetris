﻿module CopyUtility

let rec copy source target =
    let properties (x:obj) = x.GetType().GetProperties()
    query {
        for s in properties source do
        join t in properties target on (s.Name = t.Name)
        select s }
    |> Seq.iter (fun s ->
        let value = s.GetValue(source,null)
        if value.GetType().FullName.StartsWith("System.") 
        then s.SetValue(target, value, null)            
        else copy value (s.GetValue(target,null))
    )