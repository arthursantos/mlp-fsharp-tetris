﻿namespace CocosSharpTemplate.Android
open System
open CocosSharp

type IBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override iBlockInstance.State0 = array2D [|   [| 1;3|] ; [| 1;2|] ; [| 1;1|] ; [| 1;0|] |]
    override iBlockInstance.State1 = array2D [|   [| 0;1|] ; [| 1;1|] ; [| 2;1|] ; [| 3;1|] |]
    override iBlockInstance.State2 = array2D [|   [| 1;3|] ; [| 1;2|] ; [| 1;1|] ; [| 1;0|] |]
    override iBlockInstance.State3 = array2D [|   [| 0;1|] ; [| 1;1|] ; [| 2;1|] ; [| 3;1|] |]
    override iBlockInstance.GetColor = CCColor4B.Aquamarine