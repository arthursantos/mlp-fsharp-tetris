namespace CocosSharpTemplate.Android
open System
open CocosSharp

type JBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override jBlockInstance.State0 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 1;1|] ; [| 1;2|] |]
    override jBlockInstance.State1 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 2;0|] ; [| 0;1|] |]
    override jBlockInstance.State2 = array2D [|   [| 0;0|] ; [| 0;1|] ; [| 0;2|] ; [| 1;2|] |]
    override jBlockInstance.State3 = array2D [|   [| 2;0|] ; [| 0;1|] ; [| 1;1|] ; [| 2;1|] |]
    override jBlockInstance.GetColor = CCColor4B.Blue