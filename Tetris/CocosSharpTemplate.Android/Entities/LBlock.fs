
namespace CocosSharpTemplate.Android
open System
open CocosSharp

type LBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override lBlockInstance.State0 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 0;1|] ; [| 0;2|] |]
    override lBlockInstance.State1 = array2D [|   [| 0;1|] ; [| 0;2|] ; [| 1;2|] ; [| 2;2|] |]
    override lBlockInstance.State2 = array2D [|   [| 1;0|] ; [| 1;1|] ; [| 1;2|] ; [| 0;2|] |]
    override lBlockInstance.State3 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 2;0|] ; [| 2;1|] |]
    override lBlockInstance.GetColor = CCColor4B.Orange