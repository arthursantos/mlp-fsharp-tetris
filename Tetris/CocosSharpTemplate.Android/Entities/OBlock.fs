
namespace CocosSharpTemplate.Android
open System
open CocosSharp

type OBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override oBlockInstance.State0 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 0;1|] ; [| 1;1|] |]
    override oBlockInstance.State1 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 0;1|] ; [| 1;1|] |]
    override oBlockInstance.State2 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 0;1|] ; [| 1;1|] |]
    override oBlockInstance.State3 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 0;1|] ; [| 1;1|] |]
    override oBlockInstance.GetColor = CCColor4B.Yellow