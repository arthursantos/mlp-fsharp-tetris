
namespace CocosSharpTemplate.Android
open System
open CocosSharp

type SBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override sBlockInstance.State0 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 1;1|] ; [| 2;1|] |]
    override sBlockInstance.State1 = array2D [|   [| 1;2|] ; [| 1;1|] ; [| 2;1|] ; [| 2;0|] |]
    override sBlockInstance.State2 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 1;1|] ; [| 2;1|] |]
    override sBlockInstance.State3 = array2D [|   [| 1;2|] ; [| 1;1|] ; [| 2;1|] ; [| 2;0|] |]
    override sBlockInstance.GetColor = CCColor4B.Green