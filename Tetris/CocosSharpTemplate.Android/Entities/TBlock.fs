
namespace CocosSharpTemplate.Android
open System
open CocosSharp

type TBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override tBlockInstance.State0 = array2D [|   [| 1;0|] ; [| 0;1|] ; [| 1;1|] ; [| 2;1|] |]
    override tBlockInstance.State1 = array2D [|   [| 1;0|] ; [| 1;1|] ; [| 1;2|] ; [| 0;1|] |]
    override tBlockInstance.State2 = array2D [|   [| 0;0|] ; [| 1;0|] ; [| 2;0|] ; [| 1;1|] |]
    override tBlockInstance.State3 = array2D [|   [| 1;0|] ; [| 1;1|] ; [| 1;2|] ; [| 2;1|] |]
    override tBlockInstance.GetColor = CCColor4B.Magenta