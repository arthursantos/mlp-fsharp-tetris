﻿

namespace CocosSharpTemplate.Android
open System
open CocosSharp

type TetrisBlock(grid:TetrisGrid) as tetrisBlockInstance = 
    inherit CCNode()

    let constants = new Constants()
    let mutable state:int = 0
    let mutable downSpeed:int = 1

    member tetrisBlockInstance.Init() =
        tetrisBlockInstance.LoadPositions (tetrisBlockInstance.State0, 0)

    abstract member GetColor:CCColor4B
    default tetrisBlockInstance.GetColor = CCColor4B.White

    abstract member State0 : int[,]
    default tetrisBlockInstance.State0 = array2D [ ]

    abstract member State1 : int[,]
    default tetrisBlockInstance.State1 = array2D [ ]

    abstract member State2 : int[,]
    default tetrisBlockInstance.State2 = array2D [ ]

    abstract member State3 : int[,]
    default tetrisBlockInstance.State3 = array2D [ ]

    member tetrisBlockInstance.LoadPositions(positions:int[,], st:int) = 
        tetrisBlockInstance.RemoveAllChildren()
        state <- st
        for cont in 0 .. positions.GetUpperBound(0) do
            let block = new Block (tetrisBlockInstance.GetColor)
            block.Init()
            block.PositionX <- (float32 (Array2D.get positions cont 0))*constants.BLOCK_SIZE
            block.PositionY <- (float32 (Array2D.get positions cont 1))*constants.BLOCK_SIZE
            tetrisBlockInstance.AddChild (block)

        
    member tetrisBlockInstance.CheckColisionWithOffset(offsetX:float, offsetY:float):bool =
        let screenBottom = tetrisBlockInstance.VisibleBoundsWorldspace.MinY
        let screenLeft = tetrisBlockInstance.VisibleBoundsWorldspace.MinX
        let screenRight = constants.SCREEN_WIDTH
        let newY = tetrisBlockInstance.BoundingBoxTransformedToParent.MinY + (float32)offsetY
        let newX = tetrisBlockInstance.BoundingBoxTransformedToParent.MinX + (float32)offsetX
        let mutable ret = false
        for cont in 0 .. tetrisBlockInstance.Children.Count - 1 do
            let child = tetrisBlockInstance.Children.Item(cont)
            if (newY + child.BoundingBoxTransformedToParent.MinY < screenBottom) then
                ret <- true
            
            if (newX + child.BoundingBoxTransformedToParent.MinX < screenLeft) then
                ret <- true
            
            if (newX + child.BoundingBoxTransformedToParent.MaxX > screenRight) then
                ret <- true
            
            let checkX:float = (float)(newX + child.BoundingBoxTransformedToParent.MinX)
            let checkY:float = (float)(newY + child.BoundingBoxTransformedToParent.MinY)
            if (grid.EmptyPosition(checkX, checkY) = false) then
                ret <- true

        ret


    member tetrisBlockInstance.RotateRight() = 
        match state with
            | 0 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State1, 1)
            | 1 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State2, 2)
            | 2 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State3, 3)
            | 3 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State0, 0)
            | _ -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State0, 0)

        if tetrisBlockInstance.CheckColisionWithOffset (0.0, 0.0) then
            tetrisBlockInstance.RotateLeft ()

    member tetrisBlockInstance.RotateLeft() = 
        match state with
        | 0 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State3, 3)
        | 1 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State0, 0)
        | 2 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State1, 1)
        | 3 -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State2, 2)
        | _ -> tetrisBlockInstance.LoadPositions(tetrisBlockInstance.State0, 0)

        if tetrisBlockInstance.CheckColisionWithOffset (0.0, 0.0) then
            tetrisBlockInstance.RotateRight ()

    member tetrisBlockInstance.Blocks = tetrisBlockInstance.Children

    member tetrisBlockInstance.IsStoped = (downSpeed = 0)

    member tetrisBlockInstance.ParseArray (arrayOfArrays:int[,]) = 
        let lenX = Array2D.length1 arrayOfArrays
        let lenY = Array2D.length2 arrayOfArrays

        Array2D.init lenX lenY (fun i j -> Array2D.get arrayOfArrays i j) 

    member tetrisBlockInstance.DownStep() =
        let mutable colision = false

        let mutable testSpeed = 1
        while (testSpeed <= downSpeed && colision = false) do
            colision <- colision || tetrisBlockInstance.CheckColisionWithOffset (0.0, -(float)testSpeed * (float)constants.BLOCK_SIZE)
            testSpeed <- testSpeed + 1
        
        if (colision = true) then
            downSpeed <- testSpeed-2
           
        tetrisBlockInstance.PositionY <- (float32)tetrisBlockInstance.PositionY - (float32)downSpeed * (float32)constants.BLOCK_SIZE

    member tetrisBlockInstance.LeftStep() =
        if (tetrisBlockInstance.CheckColisionWithOffset (-(float)(constants.BLOCK_SIZE), (float)0.0f) = false) then
            tetrisBlockInstance.PositionX <- tetrisBlockInstance.PositionX - constants.BLOCK_SIZE
 

    member tetrisBlockInstance.RightStep() =
        if (tetrisBlockInstance.CheckColisionWithOffset ((float)constants.BLOCK_SIZE*(float)2.0f, (float)0.0f) = false) then
            tetrisBlockInstance.PositionX <- tetrisBlockInstance.PositionX + constants.BLOCK_SIZE
     

    member tetrisBlockInstance.SpeedDown() =
        downSpeed <- 5
