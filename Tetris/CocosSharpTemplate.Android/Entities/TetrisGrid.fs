﻿

namespace CocosSharpTemplate.Android
open System
open CocosSharp

type TetrisGrid() = 
    let constants = new Constants()
    let lengthY = (int)(constants.SCREEN_HEIGHT/constants.BLOCK_SIZE)
    let lengthX = (int)(constants.SCREEN_WIDTH/constants.BLOCK_SIZE)
    let mutable occupedPositions:bool[,] = Array2D.create lengthY lengthX false
    let mutable blocks:CCNode[,] = Array2D.create lengthY lengthX null

    member gridInstance.MarkBlockWithPosition(positionX:float, positionY:float, block:CCNode) =
        let col = (int)((float32)positionX / constants.BLOCK_SIZE)
        let lin = (int)((float32)positionY / constants.BLOCK_SIZE)

        if (col >= 0 && col <= occupedPositions.GetUpperBound (1) && lin >= 0 && lin <= occupedPositions.GetUpperBound (0)) then
            Array2D.set occupedPositions lin col true
            Array2D.set blocks lin col block
        else
            block.RemoveFromParent ()
    
    member gridInstance.EmptyPosition(positionX:float, positionY:float):bool =
        let col = (int)((float32)positionX / constants.BLOCK_SIZE)
        let lin = (int)((float32)positionY / constants.BLOCK_SIZE)
        let mutable ret = false
        if (col >= 0 && col <= occupedPositions.GetUpperBound (1) && lin >= 0 && lin <= occupedPositions.GetUpperBound (0)) then
            ret <- ((Array2D.get occupedPositions lin col) = false)
        else
            ret <- true
        
        ret

    member gridInstance.RemoveAllLines() = 
        let numLin = occupedPositions.GetUpperBound (0)
        let numCols = occupedPositions.GetUpperBound (1)

        for lin in 0 .. numLin do
            for col in 0 .. numCols do
                let block:CCNode = (Array2D.get blocks lin col)
                if (block <> null) then
                    block.RemoveAllChildren ()
                    block.RemoveFromParent ()
                Array2D.set blocks lin col null
                Array2D.set occupedPositions lin col false
      
    member gridInstance.RemoveCompletedLines(completedLine:int):bool =
        let numLin = occupedPositions.GetUpperBound (0)
        let numCols = occupedPositions.GetUpperBound (1)
        let mutable ret = false

        if (completedLine <= numLin) then
            //check each block of current row
            let mutable lineCompleted = true
            for col in 0 .. numCols do
                lineCompleted <- lineCompleted && (Array2D.get occupedPositions completedLine col)
                //if (lineCompleted <> true) then
                //    exit

            if (lineCompleted = true) then
                for lin in completedLine .. numLin do
                    for col in 0 .. numCols do
                        if (lin = completedLine) then
                            let block:Block = (downcast (Array2D.get blocks lin col) : Block)
                            block.RemoveAllChildren ()
                            block.RemoveFromParent ()
                            Array2D.set blocks lin col null
                            Array2D.set occupedPositions lin col false

                        if (lin < numLin) then
                            Array2D.set occupedPositions lin col (Array2D.get occupedPositions (lin + 1) col)
                            Array2D.set blocks lin col (Array2D.get blocks (lin + 1) col)
                            if ((Array2D.get blocks lin col) <> null) then
                                (Array2D.get blocks lin col).PositionY <- (Array2D.get blocks lin col).PositionY - constants.BLOCK_SIZE
                ret <- true
            else
                ret <- false
            
        ret