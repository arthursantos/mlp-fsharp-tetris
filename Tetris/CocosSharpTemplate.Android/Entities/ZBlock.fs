
namespace CocosSharpTemplate.Android
open System
open CocosSharp

type ZBlock(grid:TetrisGrid) = 
    inherit TetrisBlock(grid)

    override zBlockInstance.State0 = array2D [|   [| 0;1|] ; [| 1;1|] ; [| 1;0|] ; [| 2;0|] |]
    override zBlockInstance.State1 = array2D [|   [| 1;0|] ; [| 1;1|] ; [| 2;1|] ; [| 2;2|] |]
    override zBlockInstance.State2 = array2D [|   [| 0;1|] ; [| 1;1|] ; [| 1;0|] ; [| 2;0|] |]
    override zBlockInstance.State3 = array2D [|   [| 1;0|] ; [| 1;1|] ; [| 2;1|] ; [| 2;2|] |]
    override zBlockInstance.GetColor = CCColor4B.Red