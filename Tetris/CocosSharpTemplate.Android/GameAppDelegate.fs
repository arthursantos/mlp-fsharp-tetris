﻿namespace CocosSharpTemplate.Android

open System
open CocosSharp

type GameAppDelegate () = 
    inherit CCApplicationDelegate ()

    override this.ApplicationDidFinishLaunching (application: CCApplication, mainWindow: CCWindow) =
        application.PreferMultiSampling <- false
        application.ContentRootDirectory <- "Content"
        // Get the resolution of the main window...
        let bounds = mainWindow.WindowSizeInPixels
        let constants = new Constants()
        // ... and set that size to the CCScene object which is used by bouncing logic
        //CCScene.SetDefaultDesignResolution(bounds.Width, bounds.Height, CCSceneResolutionPolicy.ShowAll)
        CCScene.SetDefaultDesignResolution(bounds.Width, constants.SCREEN_HEIGHT, CCSceneResolutionPolicy.ShowAll)
        // todo:  Add our GameScene initialization here
        let gameScene = new GameScene (mainWindow)
        gameScene.Init()
        mainWindow.RunWithScene (gameScene)



    override this.ApplicationDidEnterBackground (application: CCApplication) =
        ()

    override this.ApplicationWillEnterForeground (application: CCApplication) =
        ()