namespace CocosSharpTemplate.Android

open System
open CocosSharp
open System.Collections.Generic
open System.Reflection
open System.Linq
open CocosSharpTemplate.Android

open CopyUtility

type GameScene (mainWindow: CCWindow) as this = 
    inherit CCScene (mainWindow)

    let mainLayer = new CCLayer ()

    let constants = new Constants()
    let touchListenerGame = new CCEventListenerTouchAllAtOnce ()
    let touchListenerMessage = new CCEventListenerTouchAllAtOnce ()

    let mutable block:TetrisBlock = Unchecked.defaultof<TetrisBlock>
    let mutable blockTypes:Type[] = Unchecked.defaultof<Type[]>
    let mutable grid:TetrisGrid = Unchecked.defaultof<TetrisGrid>
    let mutable scoreValueLabel:CCLabel = Unchecked.defaultof<CCLabel>
    let mutable score:int = 0
    let mutable lines:int = 0
    let mutable level:int = 0
    let mutable gravity:float32 = 0.8f
    let mutable messageShown:bool = false
    let mutable message:CCDrawNode = Unchecked.defaultof<CCDrawNode>

    let scoreValueLabel = new CCLabel ("0", "Arial", 50.0f)
    let scoreLabel = new CCLabel ("Score:", "Arial", 50.0f)
    let linesValueLabel = new CCLabel ("0", "Arial", 50.0f)
    let linesLabel = new CCLabel ("Lines:", "Arial", 50.0f)
    let levelValueLabel = new CCLabel ("0", "Arial", 50.0f)
    let levelLabel = new CCLabel ("Level:", "Arial", 50.0f)

    let a = Action<float32> this.RunGameLogic
    //do 
    //    this.Init()

    member this.Init() =
        mainLayer.ContentSize <- new CCSize(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT)//.//-50.0f)
        //mainLayer.VisibleBoundsWorldspace <- new CCSize(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT-50.0f)
        //mainLayer.ChildClippingMode <- CCClipMode.Bounds
        //mainLayer.Camera <- new CCCamera(new CCRect(0.0f,0.0f,constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT))
        //mainWindow.SetDesignResolutionSize (constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT, CCSceneResolutionPolicy.ExactFit)
        //CCScene.SetDefaultDesignResolution(constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT, CCSceneResolutionPolicy.ExactFit)


        this.AddChild (mainLayer)

        this.DrawRightMenu ()
        this.InitBlockTypes ()
        this.InitGrid ()
        this.AddNewTetrisBlock ()

        this.Schedule(a , gravity)

        touchListenerGame.OnTouchesMoved <- Action<_,_>(fun t te -> this.HandleTouchesMoved(t, te))
        touchListenerGame.OnTouchesEnded <- Action<_,_>(fun t te -> this.HandleTouchesEnd(t, te))

        this.AddEventListener (touchListenerGame, this)

    member this.DrawRightMenu() =
        let menu = new CCDrawNode ()


        menu.PositionX <- 0.0f
        menu.PositionY <- 0.0f

        let shape = new CCRect (0.0f, 0.0f, constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT)
        menu.DrawRect(shape, CCColor4B.LightGray, 2.0f, CCColor4B.Gray)

        mainLayer.AddChild(menu)

        scoreValueLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        scoreValueLabel.Color <- CCColor3B.White
        scoreValueLabel.PositionY <- constants.SCREEN_HEIGHT - 60.0f
        scoreValueLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (scoreValueLabel)


        scoreLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        scoreLabel.Color <- CCColor3B.White
        scoreLabel.PositionY <- constants.SCREEN_HEIGHT - 20.0f
        scoreLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (scoreLabel)



        linesValueLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        linesValueLabel.Color <- CCColor3B.White
        linesValueLabel.PositionY <- constants.SCREEN_HEIGHT - 160.0f
        linesValueLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (linesValueLabel)


        linesLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        linesLabel.Color <- CCColor3B.White
        linesLabel.PositionY <- constants.SCREEN_HEIGHT - 120.0f
        linesLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (linesLabel)



        levelValueLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        levelValueLabel.Color <- CCColor3B.White
        levelValueLabel.PositionY <- constants.SCREEN_HEIGHT - 260.0f
        levelValueLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (levelValueLabel)


        levelLabel.PositionX <- constants.SCREEN_WIDTH + 20.0f
        levelLabel.Color <- CCColor3B.White
        levelLabel.PositionY <- constants.SCREEN_HEIGHT - 220.0f
        levelLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
        mainLayer.AddChild (levelLabel)

    member this.IncrementScore(increment) =
        score <- score + increment
        scoreValueLabel.Text <- score.ToString()

    member this.ScoreLines(numLines) =
        lines <- lines + numLines
        linesValueLabel.Text <- lines.ToString()
        match numLines with 
        | 1 -> this.IncrementScore(40 * (level + 1))
        | 2 -> this.IncrementScore(40 * (level + 1))
        | 3 -> this.IncrementScore(40 * (level + 1))
        | 4 -> this.IncrementScore(40 * (level + 1))
        | _ -> ()
        this.CalculateLevel()

    member this.ScoreBlock() =
        this.IncrementScore(10 * (level + 1))

    member this.CalculateLevel() =
       let l = (lines / 10)
       if (l <> level) then
           this.CalculateGravity()
           this.Unschedule(a)
           this.Schedule(a , gravity)
           level <- l
           levelValueLabel.Text <- level.ToString()

    member this.CalculateGravity() =
        match level with
        | 0 -> gravity <- 0.8f
        | 1 -> gravity <- 0.717f
        | 2 -> gravity <- 0.633f
        | 3 -> gravity <- 0.55f
        | 4 -> gravity <- 0.467f
        | 5 -> gravity <- 0.383f
        | 6 -> gravity <- 0.3f
        | 7 -> gravity <- 0.217f
        | 8 -> gravity <- 0.133f
        | 9 -> gravity <- 0.1f
        | 10 | 11 | 12 -> gravity <- 0.08f
        | 13 | 14 | 15 -> gravity <- 0.067f
        | 16 | 17 | 18 -> gravity <- 0.05f
        | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 -> gravity <- 0.033f
        | _ -> gravity <- 0.017f
    
    member this.ResetScore() =
        score <- 0
        scoreValueLabel.Text <- score.ToString()
        lines <- 0
        linesValueLabel.Text <- lines.ToString()
        level <- 0
        levelValueLabel.Text <- level.ToString()
        gravity <- 0.8f
        this.Schedule(a , gravity)
    
    member this.InitBlockTypes() =
        blockTypes <- [| typeof<JBlock>; typeof<IBlock>; typeof<LBlock>; typeof<OBlock>; typeof<SBlock>; typeof<TBlock>; typeof<ZBlock> |]

    member this.InitGrid() =
        grid <- new TetrisGrid ()
    

    member this.RunGameLogic(interval:float32) =
        if (block.IsStoped = true) then
            this.ImportChildBlocks ()
            this.AddNewTetrisBlock ()
            if (messageShown = false) then
                this.ScoreBlock ()
        else 
            block.DownStep ()
        
        //()
 
    member this.HideEndMessage() =
        message.RemoveAllChildren ()
        message.RemoveFromParent ()
        messageShown <- false
    
    member this.ShowEndMessage() =
        if (messageShown = false) then
            messageShown <- true
            this.Unschedule(a)

            message <- new CCDrawNode ()
            message.Position <- new CCPoint (mainLayer.VisibleBoundsWorldspace.MaxX / 2.0f - 200.0f, constants.SCREEN_HEIGHT / 2.0f - 200.0f)
            message.ContentSize <- new CCSize (340.0f, 300.0f)

            let shape = new CCRect (0.0f,0.0f, 340.0f, 300.0f)
            message.DrawRect(shape, CCColor4B.White, 2.0f, CCColor4B.Black)

            let messageLabel = new CCLabel ("Game Over\nScore: " + score.ToString(), "Arial", 50.0f)
            messageLabel.PositionX <- 50.0f
            messageLabel.Color <- CCColor3B.Black
            messageLabel.PositionY <- 270.0f
            messageLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
            message.AddChild (messageLabel)

            let paddleSprite = new CCSprite ("playagain")
            paddleSprite.PositionX <- 170.0f
            paddleSprite.PositionY <- 70.0f
            paddleSprite.ContentSize <- new CCSize(250.0f, 60.0f)
            messageLabel.AnchorPoint <- CCPoint.AnchorUpperLeft
            message.AddChild (paddleSprite)

            touchListenerMessage.OnTouchesEnded <- Action<_,_>(fun t te -> this.OnRestartTouched(t, te))

            this.AddEventListener (touchListenerMessage, paddleSprite)

            mainLayer.AddChild (message)
        
    
    member this.RestartGame() =
        this.ResetScore ()
        grid.RemoveAllLines ()
        this.HideEndMessage ()
   

    member this.ImportChildBlocks() =
        let newBlocks = new List<Block> ()

        for subBlock in block.Blocks do
            let parsedBlock = downcast subBlock: Block
            let newBlock =  new Block (parsedBlock.getColor()) 
            newBlock.Init()

            newBlock.PositionX <- subBlock.PositionX + block.PositionX
            newBlock.PositionY <- subBlock.PositionY + block.PositionY

            newBlocks.Add (newBlock)

            newBlock.RemoveFromParent()
            mainLayer.AddChild (newBlock)

            grid.MarkBlockWithPosition ((float)newBlock.PositionX, (float)newBlock.PositionY, newBlock)

        let mutable LinesRemoved:int = 0
        for newBlock in newBlocks
            |> Seq.sortBy (fun (b) -> b.getLine()) do

                if (grid.RemoveCompletedLines (newBlock.getLine())) then
                    LinesRemoved <- LinesRemoved + 1
                else if(newBlock.IsOverScene()) then
                    this.ShowEndMessage ()
        if (LinesRemoved > 0) then
            this.ScoreLines (LinesRemoved)


    member this.AddNewTetrisBlock() =
        if( System.Object.Equals(block, Unchecked.defaultof<TetrisBlock>) = false) then
            block.RemoveFromParent ()

        try
            block.RemoveFromParent ()
        with
        | :? System.NullReferenceException as ex -> printfn "divide by zero"

        let rnd = new Random()
        let typeIndex = rnd.Next (0, blockTypes.Length)
        let tp:Type = Array.get blockTypes typeIndex

        block <- 
            match typeIndex with
            | 0 -> upcast new IBlock(grid):TetrisBlock
            | 1 -> upcast new JBlock(grid):TetrisBlock
            | 2 -> upcast new LBlock(grid):TetrisBlock
            | 3 -> upcast new OBlock(grid):TetrisBlock
            | 4 -> upcast new SBlock(grid):TetrisBlock
            | 5 -> upcast new TBlock(grid):TetrisBlock
            | 6 -> upcast new ZBlock(grid):TetrisBlock
            | _ -> upcast new IBlock(grid):TetrisBlock

        block.Init()

        let rest = (int (int constants.BLOCK_SIZE) % 2)
        let division = (int (int constants.SCREEN_WIDTH) / 2)

        block.PositionX <- (float32 (division - rest))
        block.PositionY <- constants.SCREEN_HEIGHT
        mainLayer.AddChild (block)

    member this.OnRestartTouched (touches:List<CCTouch>, touchEvent:CCEvent) =
        this.RestartGame ()


    member this.HandleTouchesMoved (touches: Collections.Generic.List<CCTouch>, touchEvent: CCEvent) =
        let locationOnScreen = touches.[0].LocationOnScreen
        ()

    member this.HandleTouchesEnd (touches: Collections.Generic.List<CCTouch>, touchEvent: CCEvent) =
        let locationOnScreen = touches.[0].LocationOnScreen

        if(messageShown = false) then
            touchEvent.StopPropogation ()

        if (touches.Count > 0) then
            let diff = touches.First().LocationOnScreen - touches.First().PreviousLocationOnScreen
            let angle = -diff.Angle

            //let angle = touches.First().Delta.Angle
            let point = touches.First().StartLocationOnScreen
            let distance = touches.First().LocationOnScreen.DistanceSquared(ref point)
            if (Math.Abs((float32)distance) <= 20.0f) then
                block.RotateRight ()
            else
                if ((float)angle > -Math.PI / (float)4.0f && (float)angle < Math.PI / (float)4.0f) then
                    block.RightStep ()
                else if ((float)angle > -(float)3.0f*Math.PI / (float)4.0f && (float)angle < -Math.PI / (float)4.0f) then
                    block.SpeedDown ()
                else if ((float)angle > (float)3.0f*Math.PI / (float)4.0f || (float)angle < -(float)3.0f*Math.PI / (float)4.0f) then
                    block.LeftStep ()
        ()