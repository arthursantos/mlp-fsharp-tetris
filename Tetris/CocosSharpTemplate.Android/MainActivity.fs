﻿namespace CocosSharpTemplate.Android

open System

open Android.App
open Android.Content
open Android.OS
open Android.Runtime
open Android.Views
open Android.Widget

open CocosSharp
open Android.Content.PM
open Microsoft.Xna.Framework

open System.Collections.Generic



[<Activity (Label = "BouncyEthanMaya",
            AlwaysRetainTaskState = true,
            Icon = "@drawable/icon",
            Theme = "@android:style/Theme.NoTitleBar",
            LaunchMode = LaunchMode.SingleInstance,
            ScreenOrientation = ScreenOrientation.Portrait,
            MainLauncher = true,
            ConfigurationChanges =  (ConfigChanges.Keyboard ||| ConfigChanges.KeyboardHidden))>]
type MainActivity () =
    inherit AndroidGameActivity ()

    let mutable count:int = 1

    override this.OnCreate (bundle) =
        base.OnCreate (bundle)
        let application = new CCApplication()
        application.ApplicationDelegate <- new GameAppDelegate()
        this.SetContentView(application.AndroidContentView)

        application.StartGame()

    
    